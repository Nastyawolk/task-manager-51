package ru.t1.volkova.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.command.AbstractCommand;
import ru.t1.volkova.tm.dto.request.system.DropLiquibaseSchemeRequest;
import ru.t1.volkova.tm.dto.response.system.DropLiquibaseSchemeResponse;

public final class DropLiquibaseSchemeCommand extends AbstractCommand {

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @NotNull
    private static final String DESCRIPTION = "Drop liquibase scheme.";

    @NotNull
    private static final String NAME = "drop-scheme";

    @Override
    public void execute() {
        System.out.println("[DROP SCHEME]");
        @NotNull final DropLiquibaseSchemeResponse response = getServiceLocator().
                getSystemEndpoint().dropScheme(new DropLiquibaseSchemeRequest(getToken()));
        System.out.println(response);
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }

}
