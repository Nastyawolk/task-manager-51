package ru.t1.volkova.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.dto.request.system.UpdateLiquibaseSchemeRequest;
import ru.t1.volkova.tm.dto.response.system.UpdateLiquibaseSchemeResponse;

public final class UpdateLiquibaseSchemeCommand extends AbstractSystemCommand {

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @NotNull
    private static final String DESCRIPTION = "Update liquibase scheme.";

    @NotNull
    private static final String NAME = "update-scheme";

    @Override
    public void execute() {
        System.out.println("[UPDATE SCHEME]");
        @NotNull final UpdateLiquibaseSchemeResponse response = getServiceLocator().
                getSystemEndpoint().updateScheme(new UpdateLiquibaseSchemeRequest(getToken()));
        System.out.println(response);
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }

}
