package ru.t1.volkova.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.api.repository.dto.ITaskDTORepository;
import ru.t1.volkova.tm.enumerated.Status;
import ru.t1.volkova.tm.dto.model.TaskDTO;

import java.util.Comparator;
import java.util.List;

@SuppressWarnings("UnusedReturnValue")
public interface ITaskDTOService {

    @NotNull
    TaskDTO updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

    @NotNull
    TaskDTO updateByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable String name,
            @Nullable String description
    );

    @NotNull
    TaskDTO changeTaskStatusById(
            @Nullable String userId,
            @Nullable String id,
            @NotNull Status status
    );

    @Nullable
    TaskDTO changeTaskStatusByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @NotNull Status status
    );

    @Nullable
    List<TaskDTO> findAllByProjectId(
            @Nullable String userId,
            @Nullable String projectId
    );

    @NotNull ITaskDTORepository getRepository();

    @NotNull
    TaskDTO create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description
    );

    @Nullable
    List<TaskDTO> findAll(@Nullable String userId);

    @Nullable
    List<TaskDTO> findAll(@Nullable String userId, @Nullable Comparator comparator);

    void clear(@Nullable String userId);

    @NotNull
    TaskDTO findOneById(@Nullable String userId, @Nullable String id);

    @NotNull
    TaskDTO findOneByIndex(@Nullable String userId, @Nullable Integer index);

    int getSize(@Nullable String userId);

    void removeOneById(@Nullable String userId, @Nullable String id);

    void removeOneByIndex(@Nullable String userId, @Nullable Integer index);

}
