package ru.t1.volkova.tm.api.service;

import liquibase.Liquibase;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.sql.SQLException;

public interface IConnectionService {

    @NotNull EntityManager getEntityManager();

    @NotNull
    @SneakyThrows
    Liquibase getLiquibase() throws SQLException;

    @NotNull
    EntityManagerFactory factory();

    @NotNull
    EntityManagerFactory getEntityManagerFactory();

}
