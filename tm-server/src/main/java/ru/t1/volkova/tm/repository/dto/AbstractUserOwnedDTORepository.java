package ru.t1.volkova.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.volkova.tm.api.repository.dto.IAbstractUserOwnedDTORepository;
import ru.t1.volkova.tm.dto.model.AbstractUserOwnedDTOModel;

import javax.persistence.EntityManager;

public abstract class AbstractUserOwnedDTORepository<M extends AbstractUserOwnedDTOModel>
        extends AbstractDTORepository<M> implements IAbstractUserOwnedDTORepository<M> {

    public AbstractUserOwnedDTORepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public void add(@NotNull final M entity) {
        super.add(entity);
    }

    @Override
    public void update(@NotNull final M entity) {
        super.update(entity);
    }

}
