package ru.t1.volkova.tm.endpoint;

import liquibase.Liquibase;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.volkova.tm.api.endpoint.ISystemEndpoint;
import ru.t1.volkova.tm.api.service.IPropertyService;
import ru.t1.volkova.tm.api.service.IServiceLocator;
import ru.t1.volkova.tm.dto.request.system.DropLiquibaseSchemeRequest;
import ru.t1.volkova.tm.dto.request.system.ServerAboutRequest;
import ru.t1.volkova.tm.dto.request.system.ServerVersionRequest;
import ru.t1.volkova.tm.dto.request.system.UpdateLiquibaseSchemeRequest;
import ru.t1.volkova.tm.dto.response.system.DropLiquibaseSchemeResponse;
import ru.t1.volkova.tm.dto.response.system.ServerAboutResponse;
import ru.t1.volkova.tm.dto.response.system.ServerVersionResponse;
import ru.t1.volkova.tm.dto.response.system.UpdateLiquibaseSchemeResponse;
import ru.t1.volkova.tm.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(endpointInterface = "ru.t1.volkova.tm.api.endpoint.ISystemEndpoint")
public final class SystemEndpoint extends AbstractEndPoint implements ISystemEndpoint {

    public SystemEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @NotNull
    @WebMethod
    public ServerAboutResponse getAbout(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ServerAboutRequest request
    ) {
        @NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
        @NotNull final ServerAboutResponse response = new ServerAboutResponse();
        response.setEmail(propertyService.getAuthorEmail());
        response.setName(propertyService.getAuthorName());
        return response;
    }

    @Override
    @NotNull
    @WebMethod
    public ServerVersionResponse getVersion(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ServerVersionRequest request
    ) {
        @NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
        @NotNull final ServerVersionResponse response = new ServerVersionResponse();
        response.setVersion(propertyService.getApplicationVersion());
        return response;
    }

    @Override
    @WebMethod
    @SneakyThrows
    @NotNull
    public DropLiquibaseSchemeResponse dropScheme(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DropLiquibaseSchemeRequest request
    ) {
        check(request, Role.ADMIN);
        @NotNull final Liquibase liquibase = serviceLocator.getConnectionService().getLiquibase();
        liquibase.dropAll();
        return new DropLiquibaseSchemeResponse();
    }

    @Override
    @WebMethod
    @SneakyThrows
    @NotNull
    public UpdateLiquibaseSchemeResponse updateScheme(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UpdateLiquibaseSchemeRequest request
    ) {
        check(request, Role.ADMIN);
        @NotNull final Liquibase liquibase = serviceLocator.getConnectionService().getLiquibase();
        liquibase.update("scheme");
        return new UpdateLiquibaseSchemeResponse();
    }

}
