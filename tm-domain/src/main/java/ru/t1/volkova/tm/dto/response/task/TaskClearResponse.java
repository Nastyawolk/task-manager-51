package ru.t1.volkova.tm.dto.response.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.dto.response.AbstractResponse;
import ru.t1.volkova.tm.dto.model.TaskDTO;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class TaskClearResponse extends AbstractResponse {

    @Nullable
    private List<TaskDTO> tasks;

    public TaskClearResponse(@Nullable final List<TaskDTO> tasks) {
        this.tasks = tasks;
    }

}
