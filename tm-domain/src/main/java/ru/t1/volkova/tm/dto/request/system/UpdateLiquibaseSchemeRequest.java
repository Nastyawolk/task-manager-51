package ru.t1.volkova.tm.dto.request.system;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public final class UpdateLiquibaseSchemeRequest extends AbstractUserRequest {

    public UpdateLiquibaseSchemeRequest(@Nullable final String token) {
        super(token);
    }

}
